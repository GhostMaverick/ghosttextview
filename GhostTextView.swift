//
//  GhostTextView.swift
//
//
//  Created by Ghost Maverick on 9/22/15.
//  Copyright © 2015 Ghost Maverick. All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//  1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//
//  2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
//
//  3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import Foundation
import UIKit

extension CGRect
{
    init(placeholderInRelationTo: CGRect)
    {
        self.init()
        self.origin.x = placeholderInRelationTo.origin.x
        self.origin.y = placeholderInRelationTo.origin.y + 3.0
        self.size.width = placeholderInRelationTo.size.width
        self.size.height = 21.0
    }
}

class GhostTextView: UITextView
{
    private var placeholderLabel: UILabel?
    
    var placeholderText: String? {
        set {
            self.placeholderLabel?.text = newValue
        }
        get {
            return self.placeholderLabel?.text
        }
    }
    
    var placeholderAlignment: NSTextAlignment {
        set {
           self.placeholderLabel?.textAlignment = newValue
        }
        get {
            return self.placeholderLabel!.textAlignment
        }
    }
    
    var placeholderColor: UIColor? {
        set {
            self.placeholderLabel?.textColor = newValue
        }
        get {
            return self.placeholderLabel?.textColor
        }
    }
    
    var placeholderFont: UIFont? {
        set {
            self.placeholderLabel?.font = newValue
        }
        get {
            return self.placeholderLabel?.font
        }
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
    
    init(textFrame: CGRect, placeholderFrame: CGRect)
    {
        super.init(frame: textFrame, textContainer: nil)
        self.placeholderLabel = UILabel(frame: placeholderFrame)
        self.addSubview(self.placeholderLabel!)
    }
    
    func hidePlaceholder() -> Void
    {
        self.placeholderLabel?.removeFromSuperview()
    }
}